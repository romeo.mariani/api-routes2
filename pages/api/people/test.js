//2
export default function test(req, res) {
    fetch('https://api.livecartel.com/stores')
        .then(response => response.json())
        .then(json => {
            res.status(200).json(json)
        })
        .catch(error => {
            res.status(500).json({ message: JSON.stringify(error) })
        })
}


